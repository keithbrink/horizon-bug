<?php

namespace App\Services;

use App\Exceptions\CustomException;

class ExceptionService
{
    public function handle()
    {
        try {
            throw new \Exception();
        } catch (\Exception $e) {
            throw new CustomException();
        }
    }
}
